package com.retail.core;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import junit.framework.TestCase;

@RunWith(Parameterized.class)
public class AirShipCostTest extends TestCase {
/*
	@Test
	public void test() {
		fail("Not yet implemented");
	}*/
	
	private Double expectedResult;
	private Item item;
	
	AirShipCost c;
	
	@Before
	public void setUp()	
	{
		c = new AirShipCost();
	}
	
	
	public AirShipCostTest(Double expectedResult, Item item)
	{
		this.expectedResult = expectedResult;
		this.item = item;
	}
	
	
	@Parameters
	public static Collection<Object[]> testData()
	{
		Object[][] data = new Object[][] {
			{4.63,new Item("567321101987","CD � Pink Floyd, Dark Side Of The Moon",19.99f,0.58,"AIR")},
			{4.41,new Item("567321101985","CD � Queen, A Night at the Opera",20.49f,0.55,"AIR")}
		};
		return Arrays.asList(data);
	}
	
	@Test
	public void testAirShipCost()
	{
		assertEquals(expectedResult, c.calShipCost(item), 0.01);
		
	}
 
}
