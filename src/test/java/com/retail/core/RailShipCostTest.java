package com.retail.core;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class RailShipCostTest {

	private Double expectedResult;
	private Item item;
	
	RailShipCost c;
	
	@Before
	public void setUp()	
	{
		c = new RailShipCost();
	}
	
	
	public RailShipCostTest(Double expectedResult, Item item)
	{
		this.expectedResult = expectedResult;
		this.item = item;
	}
	
	
	@Parameters
	public static Collection<Object[]> testData()
	{
		Object[][] data = new Object[][] {
			{10.0,new Item("312321101516","Hot Tub",9899.99f,793.41,"RAIL")},
			{5.0,new Item("322322202488","HeavyMac Laptop",4555.79f,4.08,"RAIL")}
		};
		return Arrays.asList(data);
	}
	
	@Test
	public void testAirShipCost()
	{
		assertEquals(expectedResult, c.calShipCost(item), 0.01);
		
	}
 

}
