package com.retail.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GroundShipCostTest {

	/*@Test
	public void test() {
		fail("Not yet implemented");
	}*/
	
//	private Item item;
    GroundShipCost gc;
	
	@Before
	public void setUp()	
	{
		gc = new GroundShipCost();
	}
	
	
	@Test
	public void testAirShipCost()
	{
		assertEquals(8.03,gc.calShipCost(new Item("477321101878","iPhone -  Headphones",17.25f,3.21,"GROUND")) , 0.01);
		
	}
	
	@Test
	public void testAirShipCost2()
	{
		assertEquals(1.53,gc.calShipCost(new Item("567321101986","CD � Beatles, Abbey Road",17.99f,0.61,"GROUND")) , 0.01);
		
	}
	

}
