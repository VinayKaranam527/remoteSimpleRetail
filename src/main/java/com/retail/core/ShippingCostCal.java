package com.retail.core;

public interface ShippingCostCal {

	public Double calShipCost(Item item);
	
}
