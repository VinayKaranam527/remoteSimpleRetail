package com.retail.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class CalculateShippingCost {

	final static Logger logger = Logger.getLogger(CalculateShippingCost.class);
	public static void main(String[] args) {
		// TODO Auto-generated method stub
  BasicConfigurator.configure();
  Double TotalShipCost=0.0;
  List<Item> itemList = new ArrayList<Item>();
  itemList.add(new Item("567321101987","CD � Pink Floyd, Dark Side Of The Moon",19.99f,0.58,"AIR"));
  itemList.add(new Item("567321101986","CD � Beatles, Abbey Road",17.99f,0.61,"GROUND"));
  itemList.add(new Item("567321101985","CD � Queen, A Night at the Opera",20.49f,0.55,"AIR"));
  itemList.add(new Item("567321101984","CD � Michael Jackson, Thriller",23.88f,0.50,"GROUND"));
  itemList.add(new Item("467321101899","iPhone - Waterproof Case",9.75f,0.73,"AIR"));
  itemList.add(new Item("477321101878","iPhone -  Headphones",17.25f,3.21,"GROUND"));
  
  class UpcCompare implements Comparator<Item>
  {
  	public int compare(Item i1, Item i2)
      {
         return i1.getUpc().compareTo(i2.getUpc());
      }
  }
  
  UpcCompare upcCompare = new UpcCompare();
  Collections.sort(itemList,upcCompare);
  ShipCostFactory sfactory = new ShipCostFactory();
  System.out.print("\t ---------Shipping Report -------\t\t");
  System.out.println("Date:"+ new Date()+"\n");
  System.out.println("UPC"+"\t\t\t"+"Description"+"\t\t"+"Price"+"\t"+"Weight"+"\t"+"Ship Method"+"\t"+"Shipping Cost"+"\n");
  
  for(Item item: itemList)
  {
	  if(item.getShippingType().equalsIgnoreCase("AIR"))
	  {
		  
	System.out.println(item.getUpc()+ "\t"+item.getDescription()+ "\t"+ 
                         item.getPrice()+"\t"+item.getWeight()+" \t"+item.getShippingType()+ "\t"+sfactory.getShipCost("AIR").calShipCost(item)
                         );
	  logger.info(item);
	  TotalShipCost += sfactory.getShipCost("AIR").calShipCost(item);
	  }
	  else if(item.getShippingType().equalsIgnoreCase("GROUND"))
		  
		  
	  { 
		  System.out.println(item.getUpc()+ "\t"+item.getDescription()+ "\t"+ 
              item.getPrice()+"\t"+item.getWeight()+"\t"+item.getShippingType()+ "\t"+sfactory.getShipCost("GROUND").calShipCost(item) );
	  TotalShipCost += sfactory.getShipCost("GROUND").calShipCost(item);
	  logger.info(item);
}
  }
  System.out.println("\n \nTotal Ship Cost:\t\t"+ TotalShipCost);
  logger.info(TotalShipCost);
  	 
	}
}






